/*
 * uartUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "uartUser.h"
#include <string.h>

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define USE_USART0_TX_DMA 0
#define USE_USART1_TX_DMA 0

#define BAUDRATE_CUS   115200						// bit per second
#define BAUDRATE_BYTE  BAUDRATE_CUS/10  // byte per second (1byte data = 1b start + 8b data + 1b stop = 10bit)
#define BFTM0_FREQ_HZ (BAUDRATE_BYTE - BAUDRATE_BYTE/4) // monitor 1.5byte
/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
buffer_t bufferRx[4];
uint8_t gRx0MainBf[RX_BUFFER_SIZE], gRx1MainBf[RX_BUFFER_SIZE];
/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
HT_USART_TypeDef *uartPort[4] = {UART0_PORT, UART1_PORT, UART2_PORT, UART3_PORT};

uint32_t sIDLE_state = 0;
PDMACH_InitTypeDef sPDMACH_TxStructure;
PDMACH_InitTypeDef sPDMACH_RxStructure;
uint8_t sRx0Buffer[RX_BUFFER_SIZE], sRx1Buffer[RX_BUFFER_SIZE];
bool sUsartIDLEint_occur = FALSE;
bool sIs0UxART_PDMA_TxBusy = FALSE, sIs1UxART_PDMA_TxBusy = FALSE;
uint8_t PDMA_Rx0_Timeout, PDMA_Rx1_Timeout;
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static void RX_ProcessNewData(uint8_t uartId);

/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void UART_Configuration(void)
{
  { /* Enable peripheral clock of AFIO, UxART                                                               */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
    CKCUClock.Bit.AFIO                   				= 1;
    CKCUClock.Bit.UART0_RX_GPIO_CLK             = 1;
		CKCUClock.Bit.UART1_RX_GPIO_CLK             = 1;
		CKCUClock.Bit.UART2_RX_GPIO_CLK             = 1;
		CKCUClock.Bit.UART3_RX_GPIO_CLK             = 1;
    CKCUClock.Bit.UART0_IPN		                  = 1;
		CKCUClock.Bit.UART1_IPN		                  = 1;
		CKCUClock.Bit.UART2_IPN		                  = 1;
		CKCUClock.Bit.UART3_IPN		                  = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }

  /* Turn on UxART Rx internal pull up resistor to prevent unknow state                                     */
  GPIO_PullResistorConfig(UART0_RX_GPIO_PORT, UART0_RX_GPIO_PIN, GPIO_PR_UP);                                
  GPIO_PullResistorConfig(UART1_RX_GPIO_PORT, UART1_RX_GPIO_PIN, GPIO_PR_UP);
	GPIO_PullResistorConfig(UART2_RX_GPIO_PORT, UART2_RX_GPIO_PIN, GPIO_PR_UP);                                
  GPIO_PullResistorConfig(UART3_RX_GPIO_PORT, UART3_RX_GPIO_PIN, GPIO_PR_UP);
	
  /* Config AFIO mode as UxART function.                                                                    */
  AFIO_GPxConfig(UART0_TX_GPIO_ID, UART0_TX_AFIO_PIN, AFIO_FUN_USART_UART); // Tx0
  AFIO_GPxConfig(UART0_RX_GPIO_ID, UART0_RX_AFIO_PIN, AFIO_FUN_USART_UART); // Rx0
  AFIO_GPxConfig(UART1_TX_GPIO_ID, UART1_TX_AFIO_PIN, AFIO_FUN_USART_UART); // Tx1
  AFIO_GPxConfig(UART1_RX_GPIO_ID, UART1_RX_AFIO_PIN, AFIO_FUN_USART_UART); // Rx1
	AFIO_GPxConfig(UART2_TX_GPIO_ID, UART2_TX_AFIO_PIN, AFIO_FUN_USART_UART); // Tx2
  AFIO_GPxConfig(UART2_RX_GPIO_ID, UART2_RX_AFIO_PIN, AFIO_FUN_USART_UART); // Rx2
  AFIO_GPxConfig(UART3_TX_GPIO_ID, UART3_TX_AFIO_PIN, AFIO_FUN_USART_UART); // Tx3
  AFIO_GPxConfig(UART3_RX_GPIO_ID, UART3_RX_AFIO_PIN, AFIO_FUN_USART_UART); // Rx3
  
	{
    /* UxART configured as follow:
          - BaudRate = 115200 baud
          - Word Length = 8 Bits
          - One Stop Bit
          - None parity bit
    */
    USART_InitTypeDef USART_InitStructure = {0};
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WORDLENGTH_8B;
    USART_InitStructure.USART_StopBits = USART_STOPBITS_1;
    USART_InitStructure.USART_Parity = USART_PARITY_NO;
    USART_InitStructure.USART_Mode = USART_MODE_NORMAL;
    USART_Init(UART0_PORT, &USART_InitStructure);
		USART_Init(UART1_PORT, &USART_InitStructure);
		USART_Init(UART2_PORT, &USART_InitStructure);
		USART_Init(UART3_PORT, &USART_InitStructure);
  }

  /* Enable UxART interrupt of NVIC                                                                         */
  NVIC_EnableIRQ(UART0_IRQn);	
	NVIC_EnableIRQ(UART1_IRQn);
	NVIC_EnableIRQ(UART2_IRQn);	
	NVIC_EnableIRQ(UART3_IRQn);
	
  /* Enable UxART Rx interrupt                                                                              */
  USART_IntConfig(UART0_PORT, USART_INT_RXDR, ENABLE);
  USART_IntConfig(UART1_PORT, USART_INT_RXDR, ENABLE);
	USART_IntConfig(UART2_PORT, USART_INT_RXDR, ENABLE);
  USART_IntConfig(UART3_PORT, USART_INT_RXDR, ENABLE);
	
  /* Enable UxART Tx and Rx function                                                                        */
  USART_TxCmd(UART0_PORT, ENABLE);
  USART_RxCmd(UART0_PORT, ENABLE);
	USART_TxCmd(UART1_PORT, ENABLE);
  USART_RxCmd(UART1_PORT, ENABLE);
	USART_TxCmd(UART2_PORT, ENABLE);
  USART_RxCmd(UART2_PORT, ENABLE);
	USART_TxCmd(UART3_PORT, ENABLE);
  USART_RxCmd(UART3_PORT, ENABLE);
}

void UART_SendByte(HT_USART_TypeDef* USARTx, uint8_t xByte)
{
    USART_SendData(USARTx, xByte);
		while(USART_GetFlagStatus(USARTx, USART_FLAG_TXC) == RESET);
}

void UART_SendString(HT_USART_TypeDef* USARTx, uint8_t *str)
{
    while(*str != 0)
    {
        UART_SendByte(USARTx, *str);
        str++; 
    }
}

/*************************************************************************************************************
  * @brief  For UxART RX
  * @retval None
  ***********************************************************************************************************/
void UART_Process(void)
{
	uint8_t i;
	for(i = 0; i < 4; i++)
	{
		if(bufferRx[i].State > 0)
		{
			bufferRx[i].State--;
			if(bufferRx[i].State == 0)
			{
				RX_ProcessNewData(i);
			}
		}
	}
}

void UART0_UART2_IRQHandler(void)
{
	uint8_t i;
	for(i = 0; i < 4; i+=2)
	{
    if(USART_GetFlagStatus(uartPort[i], USART_FLAG_RXDR))
    {
        bufferRx[i].Data[bufferRx[i].Index] = USART_ReceiveData(uartPort[i]);
        bufferRx[i].Index++;
        bufferRx[i].State = 3;
    }
	}
}

void UART1_UART3_IRQHandler(void)
{
	uint8_t i;
	for(i = 1; i < 4; i+=2)
	{
    if(USART_GetFlagStatus(uartPort[i], USART_FLAG_RXDR))
    {
        bufferRx[i].Data[bufferRx[i].Index] = USART_ReceiveData(uartPort[i]);
        bufferRx[i].Index++;
        bufferRx[i].State = 3;
    }
	}
}

static void RX_ProcessNewData(uint8_t uartId)
{
	extern void User_uartHdl(uint8_t *str);
	bufferRx[uartId].Data[bufferRx[uartId].Index] = 0;
		DEBUG("Rx[%d]: %s\r\n", uartId, bufferRx[uartId].Data);
	if(uartId == 2) User_uartHdl(bufferRx[2].Data);
	bufferRx[uartId].Index = 0;
}
/*********************************************************************************************************//**
  * @brief  Configure the PDMA.
  * @retval None
  ***********************************************************************************************************/
void USART_PDMA_Configuration(void)
{
	CKCU_ClocksTypeDef ClockFreq;
	//----------- PDMA config ---------------
  /* Enable peripheral clock of PDMA                                                                        */
  CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
  CKCUClock.Bit.PDMA   = 1;
  CKCU_PeripClockConfig(CKCUClock, ENABLE);

#if( USE_USART0_TX_DMA == 1)
  /* Tx PDMA channel configuration                                                                          */
  sPDMACH_TxStructure.PDMACH_SrcAddr = (u32)NULL;
  sPDMACH_TxStructure.PDMACH_DstAddr = (u32)&USART0_PORT->DR;
  sPDMACH_TxStructure.PDMACH_BlkCnt = 0;
  sPDMACH_TxStructure.PDMACH_BlkLen = 1;
  sPDMACH_TxStructure.PDMACH_DataSize = WIDTH_8BIT;
  sPDMACH_TxStructure.PDMACH_Priority = M_PRIO;
  sPDMACH_TxStructure.PDMACH_AdrMod = SRC_ADR_LIN_INC | DST_ADR_FIX;
  #if 0 // Config and Enable DMA when Start Tx
  PDMA_Config(USART0_TX_PDMA_CH, &gPDMACH_TxStructure);
  PDMA_EnaCmd(USART0_TX_PDMA_CH, ENABLE);
  #endif
  PDMA_IntConfig(USART0_TX_PDMA_CH, PDMA_INT_GE | PDMA_INT_TC, ENABLE);
#endif
#if( USE_USART1_TX_DMA == 1)
  /* Tx PDMA channel configuration                                                                          */
  sPDMACH_TxStructure.PDMACH_SrcAddr = (u32)NULL;
  sPDMACH_TxStructure.PDMACH_DstAddr = (u32)&USART1_PORT->DR;
  sPDMACH_TxStructure.PDMACH_BlkCnt = 0;
  sPDMACH_TxStructure.PDMACH_BlkLen = 1;
  sPDMACH_TxStructure.PDMACH_DataSize = WIDTH_8BIT;
  sPDMACH_TxStructure.PDMACH_Priority = M_PRIO;
  sPDMACH_TxStructure.PDMACH_AdrMod = SRC_ADR_LIN_INC | DST_ADR_FIX;
  #if 0 // Config and Enable DMA when Start Tx
  PDMA_Config(USART1_TX_PDMA_CH, &gPDMACH_TxStructure);
  PDMA_EnaCmd(USART1_TX_PDMA_CH, ENABLE);
  #endif
  PDMA_IntConfig(USART1_TX_PDMA_CH, PDMA_INT_GE | PDMA_INT_TC, ENABLE);
#endif

  /* Rx PDMA channel configuration                                                                          */
	sPDMACH_RxStructure.PDMACH_SrcAddr = (u32)&USART0_PORT->DR;
  sPDMACH_RxStructure.PDMACH_DstAddr = (u32)sRx0Buffer;
  sPDMACH_RxStructure.PDMACH_BlkCnt = RX_BUFFER_SIZE;
  sPDMACH_RxStructure.PDMACH_BlkLen = 1;
  sPDMACH_RxStructure.PDMACH_DataSize = WIDTH_8BIT;
  sPDMACH_RxStructure.PDMACH_Priority = H_PRIO;
  sPDMACH_RxStructure.PDMACH_AdrMod = SRC_ADR_FIX | DST_ADR_LIN_INC | AUTO_RELOAD;
  PDMA_Config(USART0_RX_PDMA_CH, &sPDMACH_RxStructure);
  PDMA_EnaCmd(USART0_RX_PDMA_CH, ENABLE);
  PDMA_IntConfig(USART0_RX_PDMA_CH, PDMA_INT_GE | PDMA_INT_BE, ENABLE);
	
	sPDMACH_RxStructure.PDMACH_SrcAddr = (u32)&USART1_PORT->DR;
  sPDMACH_RxStructure.PDMACH_DstAddr = (u32)sRx1Buffer;
  sPDMACH_RxStructure.PDMACH_BlkCnt = RX_BUFFER_SIZE;
  sPDMACH_RxStructure.PDMACH_BlkLen = 1;
  sPDMACH_RxStructure.PDMACH_DataSize = WIDTH_8BIT;
  sPDMACH_RxStructure.PDMACH_Priority = H_PRIO;
  sPDMACH_RxStructure.PDMACH_AdrMod = SRC_ADR_FIX | DST_ADR_LIN_INC | AUTO_RELOAD;
  PDMA_Config(USART1_RX_PDMA_CH, &sPDMACH_RxStructure);
  PDMA_EnaCmd(USART1_RX_PDMA_CH, ENABLE);
  PDMA_IntConfig(USART1_RX_PDMA_CH, PDMA_INT_GE | PDMA_INT_BE, ENABLE);

	NVIC_EnableIRQ(USART0_RX_PDMA_IRQ);
  NVIC_EnableIRQ(USART1_RX_PDMA_IRQ);
	
	//----------- USART config ---------------
	{ /* Enable peripheral clock of AFIO, UxART                                                               */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
    CKCUClock.Bit.AFIO                   				= 1;
		CKCUClock.Bit.USART0_RX_GPIO_CLK					  = 1;
    CKCUClock.Bit.USART1_RX_GPIO_CLK					  = 1;
		CKCUClock.Bit.USART0_IPN			         			= 1;
    CKCUClock.Bit.USART1_IPN			         			= 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }

  /* Turn on UxART Rx internal pull up resistor to prevent unknow state                                     */
  GPIO_PullResistorConfig(USART0_RX_GPIO_PORT, USART0_RX_GPIO_PIN, GPIO_PR_UP);
  GPIO_PullResistorConfig(USART1_RX_GPIO_PORT, USART1_RX_GPIO_PIN, GPIO_PR_UP);
	
  /* Config AFIO mode as UxART function.                                                                    */
  AFIO_GPxConfig(USART0_TX_GPIO_ID, USART0_TX_AFIO_PIN, AFIO_FUN_USART_UART);
  AFIO_GPxConfig(USART0_RX_GPIO_ID, USART0_RX_AFIO_PIN, AFIO_FUN_USART_UART);
  AFIO_GPxConfig(USART1_TX_GPIO_ID, USART1_TX_AFIO_PIN, AFIO_FUN_USART_UART);
  AFIO_GPxConfig(USART1_RX_GPIO_ID, USART1_RX_AFIO_PIN, AFIO_FUN_USART_UART);
	
  {
    /* UxART configured as follow:
          - BaudRate = custom baud
          - Word Length = 8 Bits
          - One Stop Bit
          - None parity bit
    */

    /* !!! NOTICE !!!
       Notice that the local variable (structure) did not have an initial value.
       Please confirm that there are no missing members in the parameter settings below in this function.
    */
    USART_InitTypeDef USART_InitStructure = {0};
    USART_InitStructure.USART_BaudRate = BAUDRATE_CUS;
    USART_InitStructure.USART_WordLength = USART_WORDLENGTH_8B;
    USART_InitStructure.USART_StopBits = USART_STOPBITS_1;
    USART_InitStructure.USART_Parity = USART_PARITY_NO;
    USART_InitStructure.USART_Mode = USART_MODE_NORMAL;
    USART_Init(USART0_PORT, &USART_InitStructure);
		USART_Init(USART1_PORT, &USART_InitStructure);
  }

  /* Enable UxART RX trigger DMA                                                                            */
  USART_PDMACmd(USART0_PORT, USART_PDMAREQ_RX, ENABLE);
	USART_PDMACmd(USART1_PORT, USART_PDMAREQ_RX, ENABLE);
	
  /* Enable UxART Tx and Rx function                                                                        */
  USART_TxCmd(USART0_PORT, ENABLE);
  USART_RxCmd(USART0_PORT, ENABLE);
	USART_TxCmd(USART1_PORT, ENABLE);
  USART_RxCmd(USART1_PORT, ENABLE);

/*	
	//----------- Basic timer config ---------------
	CKCU_GetClocksFrequency(&ClockFreq);

	BFTM_DeInit(HT_BFTM0);
	NVIC_ClearPendingIRQ(BFTM0_IRQn);
	NVIC_SetPriority(BFTM0_IRQn, 4);   
	NVIC_EnableIRQ(BFTM0_IRQn);
	{
			CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
			CKCUClock.Bit.BFTM0      = 1;
			CKCU_PeripClockConfig(CKCUClock, ENABLE);
	}
	BFTM_SetCompare(HT_BFTM0, ClockFreq.HCLK_Freq / BFTM0_FREQ_HZ - 1); 
	BFTM_SetCounter(HT_BFTM0, 0);
	BFTM_IntConfig(HT_BFTM0, ENABLE);

	BFTM_EnaCmd(HT_BFTM0, DISABLE); // enable or disable basic function timer
*/
}

/*********************************************************************************************************//**
  * @brief  UxART send a buffer by PDMA.
  * @retval None
  ***********************************************************************************************************/
u32 USART_PDMA_Tx(uint8_t usartPort, uc8 *TxBuffer, u32 length)
{
#if( USE_USART0_TX_DMA == 1)
	if(usartPort == 0)
	{
		/* Wait until previou Tx finished                                                                         */
		while (sIsUxART0_PDMA_TxBusy == TRUE);

		/* UxART Tx PDMA channel configuration                                                                    */
		sPDMACH_TxStructure.PDMACH_SrcAddr = (u32)TxBuffer;
		sPDMACH_TxStructure.PDMACH_BlkCnt = length;
		PDMA_Config(USART0_TX_PDMA_CH, &sPDMACH_TxStructure);
		PDMA_EnaCmd(USART0_TX_PDMA_CH, ENABLE);

		sIsUxART0_PDMA_TxBusy = TRUE;
		/* Enable UxART TX trigger DMA  																																					*/
		USART_PDMACmd(USART0_PORT, USART_PDMAREQ_TX, ENABLE);
	}
#endif
#if( USE_USART1_TX_DMA == 1)
	if(usartPort == 1)
	{
		/* Wait until previou Tx finished                                                                         */
		while (sIsUxART1_PDMA_TxBusy == TRUE);

		/* UxART Tx PDMA channel configuration                                                                    */
		sPDMACH_TxStructure.PDMACH_SrcAddr = (u32)TxBuffer;
		sPDMACH_TxStructure.PDMACH_BlkCnt = length;
		PDMA_Config(USART1_TX_PDMA_CH, &sPDMACH_TxStructure);
		PDMA_EnaCmd(USART1_TX_PDMA_CH, ENABLE);

		sIsUxART1_PDMA_TxBusy = TRUE;
		/* Enable UxART TX trigger DMA  																																					*/
		USART_PDMACmd(USART1_PORT, USART_PDMAREQ_TX, ENABLE);
	}
#endif
  return length;
}

/*********************************************************************************************************//**
 * @brief   This function handles PDMA interrupt.
 * @retval  None
 ************************************************************************************************************/
void USART0_PDMA_IRQHandler(void)
{
#if( USE_USART0_TX_DMA == 1)
	//--------- PDMA TX ---------
	if (PDMA_GetFlagStatus(USART0_TX_PDMA_CH, PDMA_FLAG_TC))
  {
    /* Clear interrupt flags                                                                                */
    PDMA_ClearFlag(USART0_TX_PDMA_CH, PDMA_FLAG_TC);

    sIsUxART0_PDMA_TxBusy = FALSE;
    USART_PDMACmd(USART0_PORT, USART_PDMAREQ_TX, DISABLE);
  }
#endif
	
	//--------- PDMA RX ---------
  // Check Rx PDMA Block end interrupt
  if (PDMA_GetFlagStatus(USART0_RX_PDMA_CH, PDMA_FLAG_BE))
  {
    /* Clear interrupt flags                                                                                */
    PDMA_ClearFlag(USART0_RX_PDMA_CH, PDMA_FLAG_BE);
		PDMA_Rx0_Timeout = 2;
/*		
		if(sIDLE_state == 0) 
		{
			BFTM_SetCounter(HT_BFTM0, 0);
			BFTM_EnaCmd(HT_BFTM0, ENABLE); // enable or disable basic function timer
		}
		
		sIDLE_state++;
*/
	}
}

void USART1_PDMA_IRQHandler(void)
{
#if( USE_USART1_TX_DMA == 1)
	//--------- PDMA TX ---------
	if (PDMA_GetFlagStatus(USART1_TX_PDMA_CH, PDMA_FLAG_TC))
  {
    /* Clear interrupt flags                                                                                */
    PDMA_ClearFlag(USART1_TX_PDMA_CH, PDMA_FLAG_TC);

    sIsUxART1_PDMA_TxBusy = FALSE;
    USART_PDMACmd(USART1_PORT, USART_PDMAREQ_TX, DISABLE);
  }
#endif
	
	//--------- PDMA RX ---------
  // Check Rx PDMA Block end interrupt
  if (PDMA_GetFlagStatus(USART1_RX_PDMA_CH, PDMA_FLAG_BE))
  {
    /* Clear interrupt flags                                                                                */
    PDMA_ClearFlag(USART1_RX_PDMA_CH, PDMA_FLAG_BE);
		PDMA_Rx1_Timeout = 2;
/*
		if(sIDLE_state == 0) 
		{
			BFTM_SetCounter(HT_BFTM0, 0);
			BFTM_EnaCmd(HT_BFTM0, ENABLE); // enable or disable basic function timer
		}
		
		sIDLE_state++;
*/
	}
}

/*********************************************************************************************************//**
 * @brief   This function handles BFTM0 interrupt.
 * @retval  None
 ************************************************************************************************************/
/*
void BFTM0_IRQHandler(void)
{
	uint16_t numOfByteCpy;
	// Clear Interrupt Flag First //
	BFTM_ClearFlag(HT_BFTM0);
  if(sIDLE_state > 0)
	{
		if(--sIDLE_state == 0)
		{
			numOfByteCpy = RX_BUFFER_SIZE - USART1_BUF_AVAILABLE;
			strncpy((char*)gRx0MainBf, (const char*)sRx0Buffer, numOfByteCpy);
			gRx0MainBf[numOfByteCpy] = 0;
			// -- Reset Destination Address -- //
			USART1_RESET_DESADD((u32)sRx0Buffer);
			USART1_RESET_COUNTER(RX_BUFFER_SIZE);
				
			sUsartIDLEint_occur = TRUE;
			BFTM_EnaCmd(HT_BFTM0, DISABLE); // enable or disable basic function timer
		}
	}
}
*/
/*********************************************************************************************************//**
 * @brief   This function handles USART1 DMA user.
 * @retval  None
 ************************************************************************************************************/
/*
void USART_PDMA_Process(void)
{
	extern void User_uartHdl(uint8_t *str);
	
	if(sUsartIDLEint_occur == TRUE)
	{
		sUsartIDLEint_occur = FALSE;
		DEBUG("gRxMainBf: %s", gRx1MainBf);
			
		User_uartHdl(gRx1MainBf);
	}
}
*/
void USART_PDMA_Process(void)
{
	if(PDMA_Rx0_Timeout > 0) 
	{
		if(--PDMA_Rx0_Timeout == 0)
		{
			sRx0Buffer[RX_BUFFER_SIZE - USART0_BUF_AVAILABLE] = 0;
			DEBUG("PDMA_USART0: %s", sRx0Buffer);
			USART0_RESET_DESADD((u32)sRx0Buffer);
			USART0_RESET_COUNTER(RX_BUFFER_SIZE);
		}
	}
	
	if(PDMA_Rx1_Timeout > 0) 
	{
		if(--PDMA_Rx1_Timeout == 0)
		{
			sRx1Buffer[RX_BUFFER_SIZE - USART1_BUF_AVAILABLE] = 0;
			DEBUG("PDMA_USART1: %s", sRx1Buffer);
			USART1_RESET_DESADD((u32)sRx1Buffer);
			USART1_RESET_COUNTER(RX_BUFFER_SIZE);
		}
	}
}
