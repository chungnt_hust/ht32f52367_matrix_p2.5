/*********************************************************************************************************//**
 * @file    CKCU/Clock_Configuration/main.c
 * @version $Rev:: 4434         $
 * @date    $Date:: 2019-12-20 #$
 * @brief   Main program.
 *************************************************************************************************************
 * @attention
 *
 * Firmware Disclaimer Information
 *
 * 1. The customer hereby acknowledges and agrees that the program technical documentation, including the
 *    code, which is supplied by Holtek Semiconductor Inc., (hereinafter referred to as "HOLTEK") is the
 *    proprietary and confidential intellectual property of HOLTEK, and is protected by copyright law and
 *    other intellectual property laws.
 *
 * 2. The customer hereby acknowledges and agrees that the program technical documentation, including the
 *    code, is confidential information belonging to HOLTEK, and must not be disclosed to any third parties
 *    other than HOLTEK and the customer.
 *
 * 3. The program technical documentation, including the code, is provided "as is" and for customer reference
 *    only. After delivery by HOLTEK, the customer shall use the program technical documentation, including
 *    the code, at their own risk. HOLTEK disclaims any expressed, implied or statutory warranties, including
 *    the warranties of merchantability, satisfactory quality and fitness for a particular purpose.
 *
 * <h2><center>Copyright (C) Holtek Semiconductor Inc. All rights reserved</center></h2>
 ************************************************************************************************************/

/* Includes ------------------------------------------------------------------------------------------------*/
#include "ht32.h"
#include "ht32_board.h"
#include "ht32_board_config.h"
#include "systemClockCfg.h"
#include "gpioUser.h"
#include "uartUser.h"
#include "spiUser.h"
#include "w25q16jv.h"
#include "internalFlash.h"
#include "softUart.h"
#include "rtcInternal.h"
#include "adcUser.h"
#include "i2cUser.h"
#include "ht1382.h"
#include "EN25Qxxx.h"
#include "gptmPWM.h"
#include "MatrixP25.h"
/** @addtogroup HT32_Series_Peripheral_Examples HT32 Peripheral Examples
  * @{
  */

/** @addtogroup CKCU_Examples CKCU
  * @{
  */

/** @addtogroup Clock_Configuration
  * @{
  */

/* Private constants ---------------------------------------------------------------------------------------*/
#define MAX_SOFT_TIMER 3
/* Private function prototypes -----------------------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------------------------------------*/
static uint16_t softTimer[MAX_SOFT_TIMER] = {0};

static uint8_t testTxData[8] = {0, 1, 2, 5, 6, 7, 8};
static uint8_t testRxData[8] = {0};
static adcDataType_t dat;
/* Global functions ----------------------------------------------------------------------------------------*/
/*********************************************************************************************************//**
  * @brief  Main program.
  * @retval None
  ***********************************************************************************************************/
void sysTickCallback()
{
	uint8_t i;
  for(i = 0; i < MAX_SOFT_TIMER; i++) softTimer[i]++;
	if(timeDelay > 0) timeDelay--;
}
	
	
int main(void)
{
  SysClockCfg_HSE();
//  SysClockCfg_RegCallback(sysTickCallback);
////  Gpio_Out_Configuration();
//	USART_PDMA_Configuration();
//  UART_Configuration();
//	DEBUG("\r\n---------------------------------------\r\n");
//	DEBUG("RST: %s\r\n", SystemClockCfg_GetResetReason());
//	SysClockGetCK_AHB();
//	InternalFlash_write(200, testTxData, sizeof(testTxData));
//	InternalFlash_read(200, testRxData, sizeof(testRxData));
//	DEBUG("TestRxData: %d, %d, %d, %d, %d, %d, %d, %d\r\n",  
//		testRxData[0], testRxData[1], testRxData[2], testRxData[3],
//		testRxData[4], testRxData[5], testRxData[6], testRxData[7]);
//	RtcInternal_Init();			
//	adcUser_Configuration(2);
//	I2C0_Configuration();
//	I2C0_Sendbyte(0x07, 0);
//	I2C0_Sendbyte(0x00, 88);	
//	SPI_User_Configuration();
////	SPI_User_SelfTest();
//	EN25Qxxx_selftest();
//	gptmPWM_Init();
//	WDT_Configuration(4000);
	MATRIX_Init();
	MATRIX_testFont();
  while (1)
  {
//    if(softTimer[0] >= 1)
//    {
//      softTimer[0] = 0;
//      UART_Process();
//			USART_PDMA_Process();
//			RtcInternal_Time_Show();
//    }
//    
//    if(softTimer[1] >= 1000)
//    {
//      softTimer[1] = 0;
////      Gpio_Toggle_Pin(LED1_PORT, LED1_PIN);
//			adcUser_ReadData(&dat);
//			adcUser_Convert(&dat);
//			I2C0_Readbyte(0x00);
//			WDT_Restart();
//    }	
//		
//		if(softTimer[2] >= 10)
//		{
//			softTimer[2] = 0;
//			gptmPWM_test();
//		}
  }
}

#if (HT32_LIB_DEBUG == 1)
/*********************************************************************************************************//**
  * @brief  Report both the error name of the source file and the source line number.
  * @param  filename: pointer to the source file name.
  * @param  uline: error line source number.
  * @retval None
  ***********************************************************************************************************/
void assert_error(u8* filename, u32 uline)
{
  /*
     This function is called by IP library that the invalid parameters has been passed to the library API.
     Debug message can be added here.
     Example: printf("Parameter Error: file %s on line %d\r\n", filename, uline);
  */

  while (1)
  {
  }
}
#endif

/* Private functions ---------------------------------------------------------------------------------------*/

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
