/*
 * spiUser.h
 *
 *  Created on: Nov 02, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef SPIUSER_H
#define SPIUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
/***************************  SPI0  ***************************************//**/
#define SPI0_SCK_GPIOX                    C
#define SPI0_SCK_GPION                    0
#define SPI0_MOSI_GPIOX                   A
#define SPI0_MOSI_GPION                   9
#define SPI0_MISO_GPIOX                   A
#define SPI0_MISO_GPION                   11
#define SPI0_SEL_GPIOX                    B
#define SPI0_SEL_GPION                    15
#define SPI0_IPN                         	SPI0

#define SPI0_SCK_GPIO_ID   								STRCAT2(GPIO_P,         SPI0_SCK_GPIOX)
#define SPI0_SCK_AFIO_PIN  								STRCAT2(AFIO_PIN_,      SPI0_SCK_GPION)
#define SPI0_MOSI_GPIO_ID  								STRCAT2(GPIO_P,         SPI0_MOSI_GPIOX)
#define SPI0_MOSI_AFIO_PIN 								STRCAT2(AFIO_PIN_,      SPI0_MOSI_GPION)
#define SPI0_MISO_GPIO_ID  								STRCAT2(GPIO_P,         SPI0_MISO_GPIOX)
#define SPI0_MISO_AFIO_PIN 								STRCAT2(AFIO_PIN_,      SPI0_MISO_GPION)
#define SPI0_SEL_GPIO_ID   								STRCAT2(GPIO_P,         SPI0_SEL_GPIOX)
#define SPI0_SEL_GPIO_PORT 								STRCAT2(HT_GPIO,      	SPI0_SEL_GPIOX)
#define SPI0_SEL_AFIO_PIN  								STRCAT2(AFIO_PIN_,      SPI0_SEL_GPION)
#define SPI0_SEL_GPIO_PIN  								STRCAT2(GPIO_PIN_,      SPI0_SEL_GPION)
#define SPI0_SEL_BIT_CLOCK								STRCAT2(P,      				SPI0_SEL_GPIOX)
#define SPI0_PORT													STRCAT2(HT_,            SPI0_IPN)

#define SPI0_SEL_SET     GPIO_WriteOutBits(SPI0_SEL_GPIO_PORT, SPI0_SEL_GPIO_PIN, SET); 		
#define SPI0_SEL_RESET   GPIO_WriteOutBits(SPI0_SEL_GPIO_PORT, SPI0_SEL_GPIO_PIN, RESET);  
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void SPI_User_Configuration(void);
u8 SPI_User_SendReceive(u8 data);
void SPI_User_SendMulti(u8 *p, u32 len);
void SPI_User_ReceiveMulti(u8 *p, u32 len);
void SPI_User_SelfTest(void);
#endif
#ifdef __cplusplus
}
#endif
