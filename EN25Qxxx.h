/*
 * EN25Qxxx.h
 *
 *  Created on: Jan 8, 2021
 *      Author: chungnguyen
 *
*/
#ifndef  __EN25Qxxx_H__
#define  __EN25Qxxx_H__
#include "ht32.h"

void EN25Qxxx_readData(uint32_t add, void *rx_DATA, uint32_t len);
void EN25Qxxx_writeData(uint32_t add, void *tx_DATA, uint32_t len);
void EN25Qxxx_erase4k(uint32_t add);
void EN25Qxxx_erase32k(uint32_t add);
void EN25Qxxx_erase64k(uint32_t add);
void EN25Qxxx_eraseChip(void);
uint8_t EN25Qxxx_readSR1(uint8_t bitIndex);
uint8_t EN25Qxxx_readSR2(uint8_t bitIndex);
uint8_t EN25Qxxx_readSR3(uint8_t bitIndex);
void EN25Qxxx_writeSR1(uint8_t value);
void EN25Qxxx_writeSR2(uint8_t value);
void EN25Qxxx_writeSR3(uint8_t value);

void EN25Qxxx_selftest(void);
#endif
