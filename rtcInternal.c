/*
 * rtcInternal.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "rtcInternal.h"
#include "uartUser.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define DATA_KEY_BACKUP								(0xAA55A5A5)
#define PWRCU_BAKREG_0_RTC_KEY        (PWRCU_BAKREG_0)  /* Backup Register 0                                */
#define PWRCU_BAKREG_1_RTC_SECOND     (PWRCU_BAKREG_1)  /* Backup Register 1                                */
/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */
Time_T CurTime;
/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */
static uint8_t Day_Per_Month[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
static volatile bool CK_SECOND_Flag = FALSE;
/* Definition of private datatypes go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static void RTC_Configuration(void);
static bool IsLeapYear(uint32_t year);


/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void RtcInternal_Init(void)
{
	/* Enable Backup Domain PCLK and waits until it can be accessed                                           */
  CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
  CKCUClock.Bit.BKP        = 1;
  CKCU_PeripClockConfig(CKCUClock, ENABLE);
  if (PWRCU_CheckReadyAccessed() != PWRCU_OK)
  {
    while (1);
  }

  /* NVIC configuration                                                                                     */
  NVIC_EnableIRQ(RTC_IRQn);

  /* Check the Power On Reset flag is set or not                                                            */
  if (PWRCU_GetFlagStatus() == PWRCU_FLAG_PWRPOR)
  {
    DEBUG("Power On Reset occurred!\r\n");
  }

  if (PWRCU_ReadBackupRegister((PWRCU_BAKREG_Enum) PWRCU_BAKREG_0_RTC_KEY) != DATA_KEY_BACKUP)
  {
    /* Backup data register value is not correct or not yet programmed
     (when the first time the program is executed) */

    DEBUG("RTC not yet configured!\r\n");

    /* RTC Configuration                                                                                    */
    RTC_Configuration();

    /* Adjust time and store into RTC counter                                                               */
    if (!RtcInternal_Time_Adjust(0, 0, 0, 1, 1, 2014))
    {
      DEBUG("Adjust time error!\r\n");
      while (1);
    }

    /* Enable RTC                                                                                           */
    RTC_Cmd(ENABLE);

    PWRCU_WriteBackupRegister((PWRCU_BAKREG_Enum) PWRCU_BAKREG_0_RTC_KEY, DATA_KEY_BACKUP);
  }
  else
  {
    DEBUG("No need to configure RTC!\r\n");
		
		RTC_IntConfig(RTC_INT_CSEC, ENABLE);
		/* Enable RTC                                                                                           */
    RTC_Cmd(ENABLE);
  }
}

/*********************************************************************************************************//**
  * @brief  Adjust time according to 2014.1.1 00:00:00 and store the sum of second to PWRCU_BAKREG_1 register.
  * @param  AdjustTime: pointer to Time_T structure that contains the previously configuration values.
  * @retval The status of the time adjustment, 0 (Fail) or 1 (Success)
  ***********************************************************************************************************/
uint8_t RtcInternal_Time_Adjust(uint8_t hour, uint8_t minute, uint8_t second, uint8_t day, uint8_t month, uint16_t year)
{
  uint32_t i, temp, secsum = 0;
	
  temp = year - 1;
  for (i = 0; i < (year - 2014); i++)
  {
    if (IsLeapYear(temp--) == TRUE)
    {
      secsum += (366 * 86400);
    }
    else
    {
      secsum += (365 * 86400);
    }
  }

  temp = 1;
  for (i = 0; i < (month - 1); i++)
  {
    if (temp == 2)
    {
      if (IsLeapYear(year) == TRUE)
        secsum += (29 * 86400);
      else
        secsum += (28 * 86400);
    }
    else
    {
      secsum += (Day_Per_Month[temp] * 86400);
    }
    temp++;
  }

  secsum += ((day - 1) * 86400);
  secsum += (hour * 3600 );
  secsum += (minute * 60);
  secsum += (second);

	/* Disable RTC                                                                                          */
	RTC_Cmd(DISABLE);
	
  PWRCU_WriteBackupRegister((PWRCU_BAKREG_Enum) PWRCU_BAKREG_1_RTC_SECOND, secsum);
  if (PWRCU_ReadBackupRegister((PWRCU_BAKREG_Enum) PWRCU_BAKREG_1_RTC_SECOND) != secsum)
  {
    return 0;
  }

	/* Enable RTC                                                                                           */   
	RTC_Cmd(ENABLE);
	
	DEBUG("Set dateTime: %02d:%02d:%02d %02d/%02d/%04d \r\n", hour, minute, second, day, month, year);
  return 1;
}


/*********************************************************************************************************//**
  * @brief  Calculate the current time.
  * @param  CurrentTime: pointer to Time_T structure that contains the current time values.
  * @retval The status of the time calculation, 0 (Fail) or 1 (Success)
  ***********************************************************************************************************/
uint8_t RtcInternal_Time_Get(Time_T* CurrentTime)
{
  uint32_t i, secsum = 0, temp = 0;

  secsum = PWRCU_ReadBackupRegister((PWRCU_BAKREG_Enum) PWRCU_BAKREG_1_RTC_SECOND);
  secsum += RTC_GetCounter();

  temp = 0;
  while (secsum >= (365 * 86400))
  {
    if (IsLeapYear(2014 + temp))
    {
      if (secsum >= (366 * 86400))
      {
        temp++;
        secsum -= (366 * 86400);
      }
      else
      {
        break;
      }
    }
    else
    {
      temp++;
      secsum -= (365 * 86400);
    }
  }
  CurrentTime->year = 2014 + temp;

  for (i = 1; i <= 12; i++)
  {
    if (secsum >= (Day_Per_Month[i] * 86400))
    {
      if (i == 2)  // February
      {
        if (IsLeapYear(CurrentTime->year))
        {
          if (secsum >= (29 * 86400))
            secsum -= (29 * 86400);
          else
            break;
        }
        else
        {
          secsum -= (28 * 86400);
        }
      }
      else
      {
        secsum -= (Day_Per_Month[i] * 86400);
      }
    }
    else
    {
      break;
    }
  }
  CurrentTime->month = i;

  CurrentTime->day = secsum / 86400 + 1;
  secsum -= ((CurrentTime->day - 1) * 86400);

  CurrentTime->hour = secsum / 3600;
  CurrentTime->minute = (secsum % 3600) / 60;
  CurrentTime->second = (secsum % 3600) % 60;

  return 1;
}

/*********************************************************************************************************//**
  * @brief  Shows the current time YYYY.MM.DD HH:MM:SS on the Hyperterminal.
  * @retval None
  ***********************************************************************************************************/
void RtcInternal_Time_Show(void)
{
	if (CK_SECOND_Flag)
	{
		CK_SECOND_Flag = FALSE;
		RtcInternal_Time_Get(&CurTime);

		DEBUG("Internal RTC: %04d.%02d.%02d %02d:%02d:%02d\r\n",
										CurTime.year,
										CurTime.month,
										CurTime.day,
										CurTime.hour,
										CurTime.minute,
										CurTime.second);
	}
}

/*********************************************************************************************************//**
 * @brief   This function handles RTC interrupt.
 * @retval  None
 ************************************************************************************************************/
void RTC_IRQHandler(void)
{
  uint8_t bFlags;

  bFlags = RTC_GetFlagStatus();
  if (bFlags & RTC_FLAG_CSEC)
  {
    /* time update                                                                                          */
    CK_SECOND_Flag = TRUE;
  }
}

/*********************************************************************************************************//**
 * @brief Configures RTC.
 * @retval None
 * @details RTC configuration as following:
 *  - S/W reset the Backup Domain.
 *  - Configure Low Speed External clock (LSE).
 *    - The LSE use fast startup mode. Shortly startup time but higher power consumption.
 *    - Enable the LSE. Waits till LSE is ready.
 *  - Configure RTC.
 *    - Select LSE as RTC Clock Source.
 *    - Enable the RTC Second interrupt.
 *    - RTC prescaler = 32768 to generate 1 second interrupt.
 ************************************************************************************************************/
static void RTC_Configuration(void)
{
  /* Reset Backup Domain                                                                                    */
  PWRCU_DeInit();

  /* Check LSE Status                                                                                       */
  if (GetBit_BB((uint32_t)(&HT_CKCU->GCSR), 4))
  {
    /* Configure Low Speed External clock (LSE)                                                             */
    RTC_LSESMConfig(RTC_LSESM_FAST);
    RTC_LSECmd(ENABLE);
    while (CKCU_GetClockReadyStatus(CKCU_FLAG_LSERDY) == RESET);
    RTC_ClockSourceConfig(RTC_SRC_LSE);
  }
  else
  {
    RTC_ClockSourceConfig(RTC_SRC_LSI);
  }
  RTC_IntConfig(RTC_INT_CSEC, ENABLE);
  RTC_SetPrescaler(RTC_RPRE_32768);
}

/*********************************************************************************************************//**
  * @brief  Determine if a year is a leap year or not.
    * @param  year: a specific year.
  * @retval The status of the leap year check, TRUE or FALSE.
  ***********************************************************************************************************/
static bool IsLeapYear(uint32_t year)
{
  if (((year % 4 == 0) && (year % 100 != 0) ) || (year % 400 == 0) )
    return TRUE;
  else
    return FALSE;
}
