/*
 * uartUser.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef UARTUSER_H
#define UARTUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define DEBUG_EN 1

#define RX_BUFFER_SIZE 20
#define MAX_NUM_DATA 100
typedef struct
{
	uint8_t Data[MAX_NUM_DATA];
	uint8_t Byte;
	uint8_t Index;
	uint8_t State;
} buffer_t;
/***************************  USART0  ***************************************//**/
#define USART0_TX_GPIOX                    A
#define USART0_TX_GPION                    8
#define USART0_RX_GPIOX                    A
#define USART0_RX_GPION                    10
#define USART0_IPN                         USART0

#define USART0_TX_GPIO_ID                  STRCAT2(GPIO_P,         USART0_TX_GPIOX)
#define USART0_RX_GPIO_ID                  STRCAT2(GPIO_P,         USART0_RX_GPIOX)
#define USART0_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      USART0_TX_GPION)
#define USART0_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      USART0_RX_GPION)
#define USART0_PORT                        STRCAT2(HT_,            USART0_IPN)
//#define USART0_IRQn                        
//#define USART0_IRQHandler                  
#define USART0_RX_GPIO_CLK                 STRCAT2(P,              USART0_RX_GPIOX)
#define USART0_RX_GPIO_PORT                STRCAT2(HT_GPIO,        USART0_RX_GPIOX)
#define USART0_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      USART0_RX_GPION)

/************************* PDMA USART0 ***************************************//**/
#define USART0_TX_CH											 CH1
#define USART0_RX_CH											 CH0

#define USART0_TX_PDMA_CH                  STRCAT2(PDMA_, USART0_TX_CH)
#define USART0_RX_PDMA_CH                  STRCAT2(PDMA_, USART0_RX_CH)
#define USART0_TX_PDMA_IRQ                 STRCAT3(PDMA, USART0_TX_CH, _IRQn)
#define USART0_RX_PDMA_IRQ                 STRCAT3(PDMA, USART0_RX_CH, _IRQn)
#define USART0_PDMA_IRQHandler          	 PDMA_CH0_1_IRQHandler
#define USART0_BUF_AVAILABLE    	         (HT_PDMA->PDMACH0.CTSR >> 16)
#define USART0_RESET_DESADD(add)     			 (HT_PDMA->PDMACH0.DADR = add)
#define USART0_RESET_COUNTER(BlkCnt)		   (HT_PDMA->PDMACH0.TSR = ((BlkCnt << 16) | 1))

/***************************  USART1  ***************************************//**/
#define USART1_TX_GPIOX                    A
#define USART1_TX_GPION                    4
#define USART1_RX_GPIOX                    A
#define USART1_RX_GPION                    5
#define USART1_IPN                         USART1

#define USART1_TX_GPIO_ID                  STRCAT2(GPIO_P,         USART1_TX_GPIOX)
#define USART1_RX_GPIO_ID                  STRCAT2(GPIO_P,         USART1_RX_GPIOX)
#define USART1_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      USART1_TX_GPION)
#define USART1_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      USART1_RX_GPION)
#define USART1_PORT                        STRCAT2(HT_,            USART1_IPN)
//#define USART1_IRQn                        
//#define USART1_IRQHandler                  
#define USART1_RX_GPIO_CLK                 STRCAT2(P,              USART1_RX_GPIOX)
#define USART1_RX_GPIO_PORT                STRCAT2(HT_GPIO,        USART1_RX_GPIOX)
#define USART1_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      USART1_RX_GPION)

/************************* PDMA USART1 ***************************************//**/
#define USART1_TX_CH											 CH3
#define USART1_RX_CH											 CH2

#define USART1_TX_PDMA_CH                  STRCAT2(PDMA_, USART1_TX_CH)
#define USART1_RX_PDMA_CH                  STRCAT2(PDMA_, USART1_RX_CH)
#define USART1_TX_PDMA_IRQ                 STRCAT3(PDMA, USART1_TX_CH, _IRQn)
#define USART1_RX_PDMA_IRQ                 STRCAT3(PDMA, USART1_RX_CH, _IRQn)
#define USART1_PDMA_IRQHandler          	 PDMA_CH2_5_IRQHandler
#define USART1_BUF_AVAILABLE    	         (HT_PDMA->PDMACH2.CTSR >> 16)
#define USART1_RESET_DESADD(add)     			 (HT_PDMA->PDMACH2.DADR = add)
#define USART1_RESET_COUNTER(BlkCnt)		   (HT_PDMA->PDMACH2.TSR = ((BlkCnt << 16) | 1))

/***************************   UART0  ***************************************//**/
#define UART0_TX_GPIOX                    C
#define UART0_TX_GPION                    6
#define UART0_RX_GPIOX                    C
#define UART0_RX_GPION                    7
#define UART0_IPN                         UART0

#define UART0_TX_GPIO_ID                  STRCAT2(GPIO_P,         UART0_TX_GPIOX)
#define UART0_RX_GPIO_ID                  STRCAT2(GPIO_P,         UART0_RX_GPIOX)
#define UART0_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      UART0_TX_GPION)
#define UART0_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      UART0_RX_GPION)
#define UART0_PORT                        STRCAT2(HT_,            UART0_IPN)
//#define UART0_IRQn                        
//#define UART0_IRQHandler                  
#define UART0_RX_GPIO_CLK                 STRCAT2(P,              UART0_RX_GPIOX)
#define UART0_RX_GPIO_PORT                STRCAT2(HT_GPIO,        UART0_RX_GPIOX)
#define UART0_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      UART0_RX_GPION)

/***************************   UART1  ***************************************//**/
#define UART1_TX_GPIOX                    D
#define UART1_TX_GPION                    4
#define UART1_RX_GPIOX                    D
#define UART1_RX_GPION                    5
#define UART1_IPN                         UART1

#define UART1_TX_GPIO_ID                  STRCAT2(GPIO_P,         UART1_TX_GPIOX)
#define UART1_RX_GPIO_ID                  STRCAT2(GPIO_P,         UART1_RX_GPIOX)
#define UART1_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      UART1_TX_GPION)
#define UART1_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      UART1_RX_GPION)
#define UART1_PORT                        STRCAT2(HT_,            UART1_IPN)
//#define UART1_IRQn                        
//#define UART1_IRQHandler                  
#define UART1_RX_GPIO_CLK                 STRCAT2(P,              UART1_RX_GPIOX)
#define UART1_RX_GPIO_PORT                STRCAT2(HT_GPIO,        UART1_RX_GPIOX)
#define UART1_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      UART1_RX_GPION)

/***************************   UART2  ***************************************//**/
#define UART2_TX_GPIOX                    B
#define UART2_TX_GPION                    2
#define UART2_RX_GPIOX                    B
#define UART2_RX_GPION                    3
#define UART2_IPN                         UART2

#define UART2_TX_GPIO_ID                  STRCAT2(GPIO_P,         UART2_TX_GPIOX)
#define UART2_RX_GPIO_ID                  STRCAT2(GPIO_P,         UART2_RX_GPIOX)
#define UART2_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      UART2_TX_GPION)
#define UART2_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      UART2_RX_GPION)
#define UART2_PORT                        STRCAT2(HT_,            UART2_IPN)
//#define UART2_IRQn                        
//#define UART2_IRQHandler                  
#define UART2_RX_GPIO_CLK                 STRCAT2(P,              UART2_RX_GPIOX)
#define UART2_RX_GPIO_PORT                STRCAT2(HT_GPIO,        UART2_RX_GPIOX)
#define UART2_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      UART2_RX_GPION)

/***************************   UART3  ***************************************//**/
#define UART3_TX_GPIOX                    C
#define UART3_TX_GPION                    14
#define UART3_RX_GPIOX                    C
#define UART3_RX_GPION                    15
#define UART3_IPN                         UART3

#define UART3_TX_GPIO_ID                  STRCAT2(GPIO_P,         UART3_TX_GPIOX)
#define UART3_RX_GPIO_ID                  STRCAT2(GPIO_P,         UART3_RX_GPIOX)
#define UART3_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      UART3_TX_GPION)
#define UART3_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      UART3_RX_GPION)
#define UART3_PORT                        STRCAT2(HT_,            UART3_IPN)
//#define UART3_IRQn                        
//#define UART3_IRQHandler                  
#define UART3_RX_GPIO_CLK                 STRCAT2(P,              UART3_RX_GPIOX)
#define UART3_RX_GPIO_PORT                STRCAT2(HT_GPIO,        UART3_RX_GPIOX)
#define UART3_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      UART3_RX_GPION)
#if(DEBUG_EN == 1)
#define DEBUG(str, ...) printf(str, ##__VA_ARGS__)
#else
#define DEBUG(str, ...)
#endif
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
/*************************************************************************************************************
  * @brief  Configure the UART
  * @retval None
  ***********************************************************************************************************/
void UART_Configuration(void);

/*************************************************************************************************************
  * @brief  Transmit data
  * @retval None
  ***********************************************************************************************************/
void UART_SendByte(HT_USART_TypeDef* USARTx, uint8_t xByte);
void UART_SendString(HT_USART_TypeDef* USARTx, uint8_t *str);

/*************************************************************************************************************
  * @brief  Process Received data
  * @retval None
  ***********************************************************************************************************/
void UART_Process(void);

/*************************************************************************************************************
  * @brief  Configuration and process function USART1 
  * @retval None
  ***********************************************************************************************************/
void USART_PDMA_Configuration(void);
void USART_PDMA_Process(void);
#endif
#ifdef __cplusplus
}
#endif
