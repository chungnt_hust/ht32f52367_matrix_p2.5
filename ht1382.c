/*
 * ht1382.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht1382.h"
#include "softI2C.h"


/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
static uint8_t DEC_BCD(uint8_t DEC)
{
	uint8_t temp = DEC;
	return (((DEC/10)*16) + (temp%10));
}
/*---------------*/
static uint8_t BCD_DEC(uint8_t BCD)
{
	uint8_t temp = BCD;
	return ((BCD/16)*10 + temp%16);
}

/*********************************************************************************************************//**
  * @brief  HT1382_Init.
  * @retval None
  ***********************************************************************************************************/
void HT1382_Init(void)
{
	softI2C_Init(HT1382_ADDR);
	HT1382_Write(HT1382_WPR, 0);
	HT1382_Write(HT1382_SECR, 50);
}

/*********************************************************************************************************//**
  * @brief  HT1382_Read_Datetime.
  * @retval None
  ***********************************************************************************************************/
DateTime_TypeDef HT1382_Read_Datetime(void)
{
	DateTime_TypeDef rtc;
	uint8_t Sec, Min, Hour, Date, Day, Month, Year;
	softI2C_Start();
	softI2C_Send_Byte(HT1382_ADDR);
	softI2C_Send_Byte(HT1382_SECR);
	softI2C_Start();
	softI2C_Send_Byte(HT1382_ADDR | 1);
	Sec   = softI2C_Receive_Byte(RTC_ACK);
	Min   = softI2C_Receive_Byte(RTC_ACK);
	Hour  = softI2C_Receive_Byte(RTC_ACK);
	Date  = softI2C_Receive_Byte(RTC_ACK);
	Day   = softI2C_Receive_Byte(RTC_ACK);
	Month = softI2C_Receive_Byte(RTC_ACK);
	Year  = softI2C_Receive_Byte(RTC_NACK);
	softI2C_Stop();	
	rtc.Seconds = Sec;
	rtc.Minutes = Min;
	rtc.Hours 	= Hour;
	rtc.Day 		= Day;
	rtc.Month 	= Month;
	rtc.Year 		= Year;
	rtc.Date 		= Date;
	return rtc;
}

/*********************************************************************************************************//**
  * @brief  HT1382_Write_Datetime.
  * @retval None
  ***********************************************************************************************************/
void HT1382_Write_Datetime(DateTime_TypeDef rtc)
{
	softI2C_Start();
	softI2C_Send_Byte(HT1382_ADDR);
	softI2C_Send_Byte(HT1382_SECR);
	softI2C_Send_Byte(rtc.Seconds);		// sec
	softI2C_Send_Byte(rtc.Minutes);		// min
	softI2C_Send_Byte(rtc.Hours);   	// hour
	softI2C_Send_Byte(rtc.Date);			// day on week
	softI2C_Send_Byte(rtc.Day);	  		// day
	softI2C_Send_Byte(rtc.Month);	  	// month
	softI2C_Send_Byte(rtc.Year);	  	// year	
	softI2C_Stop();
}

/*********************************************************************************************************//**
  * @brief  HT1382_Read.
  * @retval None
  ***********************************************************************************************************/
uint8_t HT1382_Read(uint8_t add)
{
	uint8_t tem = softI2C_Read(add);
	return BCD_DEC(tem);
}

/*********************************************************************************************************//**
  * @brief  HT1382_Write.
  * @retval None
  ***********************************************************************************************************/
void HT1382_Write(uint8_t add, uint8_t data)
{
	uint8_t tem = DEC_BCD(data);
	softI2C_Write(add, tem);
}
