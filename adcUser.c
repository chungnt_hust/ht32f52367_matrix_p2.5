/*
 * adcUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "adcUser.h"
#include "uartUser.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define USE_INT 											0
#define USE_IVREF_OUT									0

#define ADC_VREF_LEVEL                ADC_VREF_1V215
#define VREF_VALUE_mV                 (1215)

//#define ADC_VREF_LEVEL                ADC_VREF_2V0
//#define VREF_VALUE_mV                 (2000)

//#define ADC_VREF_LEVEL                ADC_VREF_2V5
//#define VREF_VALUE_mV                 (2500)

//#define ADC_VREF_LEVEL                ADC_VREF_2V7
//#define VREF_VALUE_mV                 (2700)
/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
static uint8_t numOfChannel = 0;
static volatile bool adcEOC = FALSE;
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
  * @brief  ADC configuration.
  * @retval None
  ***********************************************************************************************************/
void adcUser_Configuration(uint8_t numCH)
{
	numOfChannel = numCH;
  { /* Enable peripheral clock                                                                              */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
    CKCUClock.Bit.AFIO = 1;
    CKCUClock.Bit.ADC0 = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }

  /* Configure AFIO mode as ADC and VREF function                                                           */
#if(USE_IVREF_OUT == 1) 
	AFIO_GPxConfig(VREF_GPIO_ID, VREF_AFIO_PIN, AFIO_FUN_SYSTEM);
#endif
	AFIO_GPxConfig(VR0_GPIO_ID, VR0_AFIO_PIN, AFIO_FUN_ADC0);
  

  { /* ADC related settings                                                                                 */
    /* CK_ADC frequency is set to (CK_AHB / 64)                                                             */
    CKCU_SetADCnPrescaler(CKCU_ADCPRE_ADC0, CKCU_ADCPRE_DIV64);

    /* One Shot mode, sequence length = numCH                                                                   */
    ADC_RegularGroupConfig(HT_ADC0, ONE_SHOT_MODE, numCH, 0);

    /* ADC conversion time = (Sampling time + Latency) / CK_ADC = (1.5 + ADST + 12.5) / CK_ADC              */
    /* Set ADST = 36, sampling time = 1.5 + ADST                                                            */
    ADC_SamplingTimeConfig(HT_ADC0, 36);

    /* Set ADC conversion sequence as channel n                                                             */
    ADC_RegularChannelConfig(HT_ADC0, ADC_CH_IVREF, 0, 36);
    ADC_RegularChannelConfig(HT_ADC0, VR0_ADC_CH, 1, 36);
		
		/* Set software trigger as ADC trigger source                                                           */
    ADC_RegularTrigConfig(HT_ADC0, ADC_TRIG_SOFTWARE);
  }

  /* Select the internal VREF output level and enable it                                                    */
  ADC_VREFConfig(HT_ADC0, ADC_VREF_LEVEL);
  ADC_VREFCmd(HT_ADC0, ENABLE);
	
	/* Enable ADC     																																												*/							
	ADC_Cmd(HT_ADC0, ENABLE);
	
#if(USE_INT == 1)
	/* Enable ADC single end of conversion interrupt                                                          */
  ADC_IntConfig(HT_ADC0, ADC_INT_CYCLE_EOC, ENABLE);

  /* Enable the ADC interrupts                                                                              */
  NVIC_EnableIRQ(ADC0_IRQn);
#endif
}

/*********************************************************************************************************//**
 * @brief   This function handles ADC interrupt.
 * @retval  None
 ************************************************************************************************************/
#if(USE_INT == 1)
void ADC_IRQHandler(void)
{
  if(ADC_GetIntStatus(HT_ADC0, ADC_INT_CYCLE_EOC) == SET)
  {
    ADC_ClearIntPendingBit(HT_ADC0, ADC_FLAG_CYCLE_EOC);
		adcEOC = TRUE;
	}
}
#endif
/*********************************************************************************************************//**
  * @brief  ADC read raw data.
  * @retval None
  ***********************************************************************************************************/
void adcUser_ReadData(adcDataType_t *dat)
{
	uint8_t i;
	ADC_SoftwareStartConvCmd(HT_ADC0, ENABLE);
//	DEBUG("ADC start conv\r\n");
//	for(i = 0; i < numOfChannel; i++)
//	{
//		while(ADC_GetFlagStatus(HT_ADC0, ADC_FLAG_SINGLE_EOC) == RESET);
//		ADC_ClearIntPendingBit(HT_ADC0, ADC_FLAG_SINGLE_EOC);
//	}
#if(USE_INT == 1)
	while(adcEOC == FALSE);
	adcEOC = FALSE;
#else
	while(ADC_GetFlagStatus(HT_ADC0, ADC_FLAG_CYCLE_EOC) == RESET);
	ADC_ClearIntPendingBit(HT_ADC0, ADC_FLAG_CYCLE_EOC);
#endif
//	DEBUG("ADC end conv\r\n");
	for(i = 0; i < numOfChannel; i++) dat->convResult[i] = HT_ADC0->DR[i] & 0x0FFF;
}

/*********************************************************************************************************//**
  * @brief  ADC convert data.
  * @retval None
  ***********************************************************************************************************/
void adcUser_Convert(adcDataType_t *dat)
{
	DEBUG("VDDA =   %4dmV  \n\r", (VREF_VALUE_mV * 4095) / dat->convResult[0]);
	DEBUG("VR   =   %4dmV  \n\r", (VREF_VALUE_mV * dat->convResult[1]) / dat->convResult[0]);
}
