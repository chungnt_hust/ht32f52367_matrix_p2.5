/*
 * gptmPWM.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef GPTMPWM_H
#define GPTMPWM_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define GT_PWM_IPN                         			GPTM0

#define GT_PWM_CH0_CHN                         	0
#define GT_PWM_CH0_GPIOX                       	A
#define GT_PWM_CH0_GPION                       	0

#define GT_PWM_CH1_CHN                         	1
#define GT_PWM_CH1_GPIOX                       	A
#define GT_PWM_CH1_GPION                       	1

#define GT_PWM_CH2_CHN                         	2
#define GT_PWM_CH2_GPIOX                       	A
#define GT_PWM_CH2_GPION                       	6

#define GT_PWM_CH3_CHN                         	3
#define GT_PWM_CH3_GPIOX                       	A
#define GT_PWM_CH3_GPION                       	3

#define GT_PWM_PORT                         		STRCAT2(HT_,       GT_PWM_IPN)
#define GT_PWM_AFIO_FUN                     		STRCAT2(AFIO_FUN_, GT_PWM_IPN)
#define GT_PWM_IRQn                         		STRCAT2(GT_PWM_IPN,  _IRQn)
#define GT_PWM_IRQHandler                   		STRCAT2(GT_PWM_IPN,  _IRQHandler)

#define PWM_CH2_GPIO_ID                         STRCAT2(GPIO_P,         GT_PWM_CH2_GPIOX)
#define PWM_CH2_AFIO_PIN                        STRCAT2(AFIO_PIN_,      GT_PWM_CH2_GPION)
#define PWM_CH2_CH                              STRCAT2(TM_CH_,         GT_PWM_CH2_CHN)
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void gptmPWM_Init(void);
/*********************************************************************************************************//**
  * @brief  Set PWM Frequency
  * @param  uReload: Reload value of timer)
  * @retval None
  ***********************************************************************************************************/
void gptmPWM_SetFreq(u32 uReload);
/*********************************************************************************************************//**
  * @brief  Update PWM Duty
  * @param  TM_CH_n: Specify the TM channel.
  * @param  uCompare: PWM duty (Compare value of timer)
  * @retval None
  ***********************************************************************************************************/
void gptmPWM_UpdateDuty(TM_CH_Enum TM_CH_n, u32 uCompare);
/*********************************************************************************************************//**
  * @brief Enable or Disable PWM.
  * @param NewState: This parameter can be ENABLE or DISABLE.
  * @retval None
  ***********************************************************************************************************/
void gptmPWM_Cmd(ControlStatus NewState);

void gptmPWM_test(void);
#endif
#ifdef __cplusplus
}
#endif
