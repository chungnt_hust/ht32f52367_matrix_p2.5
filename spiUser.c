/*
 * spiUser.c
 *
 *  Created on: Nov 02, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "spiUser.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void SPI_User_Configuration(void)
{
  /* !!! NOTICE !!!
     Notice that the local variable (structure) did not have an initial value.
     Please confirm that there are no missing members in the parameter settings below in this function.
  */
  SPI_InitTypeDef SPI_InitStructure;

  CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
  CKCUClock.Bit.SPI0_IPN       = 1;
	CKCUClock.Bit.SPI0_SEL_BIT_CLOCK = 1;
  CKCUClock.Bit.AFIO           = 1;
  CKCU_PeripClockConfig(CKCUClock, ENABLE);


  AFIO_GPxConfig(SPI0_SCK_GPIO_ID,  SPI0_SCK_AFIO_PIN,  AFIO_FUN_SPI);
  AFIO_GPxConfig(SPI0_MOSI_GPIO_ID, SPI0_MOSI_AFIO_PIN, AFIO_FUN_SPI);
  AFIO_GPxConfig(SPI0_MISO_GPIO_ID, SPI0_MISO_AFIO_PIN, AFIO_FUN_SPI);
//   AFIO_GPxConfig(GPIO_PA, AFIO_PIN_3, AFIO_FUN_SPI);

  SPI_InitStructure.SPI_Mode = SPI_MASTER;
  // SPI_InitStructure.SPI_FIFO = SPI_FIFO_ENABLE;
  SPI_InitStructure.SPI_FIFO = SPI_FIFO_DISABLE;
  SPI_InitStructure.SPI_DataLength = SPI_DATALENGTH_8;
  // SPI_InitStructure.SPI_SELMode = SPI_SEL_HARDWARE;
  SPI_InitStructure.SPI_SELMode = SPI_SEL_SOFTWARE;
  SPI_InitStructure.SPI_SELPolarity = SPI_SELPOLARITY_LOW;
  SPI_InitStructure.SPI_FirstBit = SPI_FIRSTBIT_MSB;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_HIGH;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_SECOND;
  // SPI_InitStructure.SPI_RxFIFOTriggerLevel = 1;
  SPI_InitStructure.SPI_RxFIFOTriggerLevel = 0;
  SPI_InitStructure.SPI_TxFIFOTriggerLevel = 0;
  SPI_InitStructure.SPI_ClockPrescaler = 8;
  SPI_Init(SPI0_PORT, &SPI_InitStructure);

  SPI_SELOutputCmd(SPI0_PORT, ENABLE);

  // SPI_IntConfig(SPI_PORT, SPI_INT_RXBNE, ENABLE);
  // NVIC_EnableIRQ(SPI_IRQ);

  SPI_Cmd(SPI0_PORT, ENABLE);

  AFIO_GPxConfig(SPI0_SEL_GPIO_ID, SPI0_SEL_AFIO_PIN, AFIO_FUN_GPIO);
  GPIO_DirectionConfig(SPI0_SEL_GPIO_PORT, SPI0_SEL_GPIO_PIN, GPIO_DIR_OUT);
	GPIO_WriteOutBits(SPI0_SEL_GPIO_PORT, SPI0_SEL_GPIO_PIN, SET);
}

u8 SPI_User_SendReceive(u8 data)
{
  /* Loop while Tx buffer register is empty                                                                 */
  while (!SPI_GetFlagStatus(SPI0_PORT, SPI_FLAG_TXBE));
  SPI_SendData(SPI0_PORT, data);
  /* Loop while Rx is not empty                                                                             */
  while (!SPI_GetFlagStatus(SPI0_PORT, SPI_FLAG_RXBNE));
	return (SPI_ReceiveData(SPI0_PORT));
}

void SPI_User_SendMulti(u8 *p, u32 len)
{
  while(len--) SPI_User_SendReceive(*p++);
}

void SPI_User_ReceiveMulti(u8 *p, u32 len)
{
  u32 i = 0;
  for(i = 0; i < len; i++) p[i] = SPI_User_SendReceive(0);
}

static uint8_t testData = 0xAA;
void SPI_User_SelfTest(void)
{
	testData = SPI_User_SendReceive(0xBB);
}
