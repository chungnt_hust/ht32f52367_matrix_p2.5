/*
 * EN25Qxxx.c
 *
 *  Created on: Jan 8, 2021
 *      Author: chungnt@epi-tech.com.vn
 */
/**** include: EN25Qxxx.h ****/
#include "EN25Qxxx.h"
#include "spiUser.h"
#include "gpioUser.h"
#include "systemClockCfg.h"
#include "uartUser.h"

static uint8_t writeEN     = 0x06; // Lenh cho phep ghi
static uint8_t writeDIS    = 0x04; // Lenh ko cho phep viet
static uint8_t readDAT     = 0x03; // Lenh doc du lieu
static uint8_t writePage   = 0x02; // Cho phep viet tu 1 byte den 256 byte
static uint8_t sector4k    = 0x20; // Lenh xoa bo nho trong 1 sector 4k-byte
static uint8_t block32k    = 0x52; // Lenh xoa bo nho trong 1 block 42k-byte
static uint8_t block64k    = 0xd8; // Lenh xoa bo nho trong 1 sector 64k-byte
static uint8_t chipEra     = 0x60; // Lenh xoa chip
static uint8_t rSR1        = 0x05; // Lenh doc gia tri thanh ghi trang thai SR1
static uint8_t wSR1        = 0x01; // Lenh cho phep ghi thanh ghi trang thai SR1
static uint8_t rSR2        = 0x35;
static uint8_t wSR2        = 0x31;
static uint8_t rSR3        = 0x15;
static uint8_t wSR3        = 0x11;


// Ham gui lenh cho phep ghi 
static void EN25Qxxx_writeEnable()
{
	SPI0_SEL_RESET;
	SPI_User_SendMulti(&writeEN, 1);
	SPI0_SEL_SET; 
	// Chip Select (CS#) must be driven High after the last bit of the instruction sequence has been shifted in
}

// Ham gui lenh ko cho phep ghi
static void EN25Qxxx_writeDisable()
{
	SPI0_SEL_RESET;
	SPI_User_SendMulti(&writeDIS, 1);
	SPI0_SEL_SET;
}

// Ham doc du lieu
void EN25Qxxx_readData(uint32_t add, void *rx_DATA, uint32_t len)
{
	uint8_t *p;
	uint8_t arr[3];
	arr[0] = (add&0x00FF0000)>>16;
	arr[1] = (add&0x0000FF00)>>8;
	arr[2] = add&0x000000FF;
	
	SPI0_SEL_RESET;
	SPI_User_SendMulti(&readDAT, 1);
	SPI0_SEL_SET;
	SPI0_SEL_RESET;
	SPI_User_SendMulti(arr, 3);
	
	p = (uint8_t*)rx_DATA;
	SPI_User_ReceiveMulti(p, len);
	
	SPI0_SEL_SET;
	//HAL_Delay(100);
	SysClockDelay_ms(100);
}

/* Neu ghi nhieu byte can chu y den vi tri cua byte dang ghi o vi tri nao cua 1 page
 * neu > 255 thi no se quay lai dau page va ghi de du lieu len => Nen ghi tu dia chi 
 * co byte cuoi la 00h
 * Neu ghi 1 byte thi thoai mai :D */ 
static void EN25Qxxx_writePage(uint32_t add, uint8_t *tx_DATA, uint32_t len)
{
	uint8_t arr[3];
	arr[0] = (add&0x00FF0000)>>16;
	arr[1] = (add&0x0000FF00)>>8;
	arr[2] = add&0x000000FF;
	
	EN25Qxxx_writeEnable();
	SPI0_SEL_RESET;
	SPI_User_SendMulti(&writePage, 1);
	SPI0_SEL_SET;
	SPI0_SEL_RESET;
	SPI_User_SendMulti(arr, 3);
	
	SPI_User_SendMulti(tx_DATA, len);
	
	SPI0_SEL_SET;
	EN25Qxxx_writeDisable();
	// HAL_Delay(100);
	SysClockDelay_ms(100);
}

void EN25Qxxx_writeData(uint32_t add, void *tx_DATA, uint32_t len)
{
	uint32_t offsetAdd;
	uint32_t luongDuLieuPhaiGhi;
	uint32_t luongDuLieuDaGhi = 0;
	uint8_t *p;
	
	while(len)
	{
		offsetAdd = add%256;
		if(offsetAdd + len > 256) luongDuLieuPhaiGhi = 256 - offsetAdd;
		else luongDuLieuPhaiGhi = len;
			
		p = (uint8_t*)tx_DATA;
		EN25Qxxx_writePage(add, (uint8_t*)&p[luongDuLieuDaGhi], luongDuLieuPhaiGhi);
		add += luongDuLieuPhaiGhi;
		luongDuLieuDaGhi += luongDuLieuPhaiGhi;
		len -= luongDuLieuPhaiGhi;		
	}
}

void EN25Qxxx_erase4k(uint32_t add)
{
	uint8_t arr[3];
	arr[0] = (add&0x00FF0000)>>16;
	arr[1] = (add&0x0000FF00)>>8;
	arr[2] = add&0x000000FF;
	
	EN25Qxxx_writeEnable();
	SPI0_SEL_RESET;
	SPI_User_SendMulti(&sector4k, 1);
	SPI0_SEL_SET;
	SPI0_SEL_RESET;
	SPI_User_SendMulti(arr, 3);
	SPI0_SEL_SET;
	EN25Qxxx_writeDisable();
	// HAL_Delay(100);
	SysClockDelay_ms(100);
}

void EN25Qxxx_erase32k(uint32_t add)
{
	uint8_t arr[3];
	arr[0] = (add&0x00FF0000)>>16;
	arr[1] = (add&0x0000FF00)>>8;
	arr[2] = add&0x000000FF;
	
	EN25Qxxx_writeEnable();
	SPI0_SEL_RESET;
	SPI_User_SendMulti(&block32k, 1);
	SPI_User_SendMulti(arr, 3);
	SPI0_SEL_SET;
	EN25Qxxx_writeDisable();
	// HAL_Delay(500);
	SysClockDelay_ms(500);
}

void EN25Qxxx_erase64k(uint32_t add)
{
	uint8_t arr[3];
	arr[0] = (add&0x00FF0000)>>16;
	arr[1] = (add&0x0000FF00)>>8;
	arr[2] = add&0x000000FF;
	
	EN25Qxxx_writeEnable();
	SPI0_SEL_RESET;
	SPI_User_SendMulti(&block64k, 1);
	SPI_User_SendMulti(arr, 3);
	SPI0_SEL_SET;
	EN25Qxxx_writeDisable();
	// HAL_Delay(1000);
	SysClockDelay_ms(1000);
}

void EN25Qxxx_eraseChip(void)
{
	EN25Qxxx_writeEnable();
	SPI0_SEL_RESET;
	SPI_User_SendMulti(&chipEra, 1);
	SPI0_SEL_SET;
	EN25Qxxx_writeDisable();
	// HAL_Delay(2000);
	SysClockDelay_ms(2000);
}

uint8_t EN25Qxxx_readSR1(uint8_t bitIndex)
{
	uint8_t value = 0;
	SPI0_SEL_RESET;
	SPI_User_SendReceive(rSR1);
	value = SPI_User_SendReceive(0);
	value = (value>>bitIndex)&0x01;
	return value;
}

uint8_t EN25Qxxx_readSR2(uint8_t bitIndex)
{
	uint8_t value = 0;
	SPI0_SEL_RESET;
	SPI_User_SendReceive(rSR2);
	value = SPI_User_SendReceive(0);
	value = (value>>bitIndex)&0x01;
	return value;
}

uint8_t EN25Qxxx_readSR3(uint8_t bitIndex)
{
	uint8_t value = 0;
	SPI0_SEL_RESET;
	SPI_User_SendReceive(rSR3);
	value = SPI_User_SendReceive(0);
	value = (value>>bitIndex)&0x01;
	return value;
}

void EN25Qxxx_writeSR1(uint8_t value)
{
	EN25Qxxx_writeEnable();
	SPI0_SEL_RESET;
	SPI_User_SendReceive(wSR1);
	SPI_User_SendReceive(value);
	SPI0_SEL_SET;
	EN25Qxxx_writeDisable();
}

void EN25Qxxx_writeSR2(uint8_t value)
{
	EN25Qxxx_writeEnable();
	SPI0_SEL_RESET;
	SPI_User_SendReceive(wSR2);
	SPI_User_SendReceive(value);
	SPI0_SEL_SET;
	EN25Qxxx_writeDisable();
}

void EN25Qxxx_writeSR3(uint8_t value)
{
	EN25Qxxx_writeEnable();
	SPI0_SEL_RESET;
	SPI_User_SendReceive(wSR3);
	SPI_User_SendReceive(value);
	SPI0_SEL_SET;
	EN25Qxxx_writeDisable();
}

typedef struct
{
	uint8_t x1;
	uint32_t x2;
} dataType_t;
static dataType_t tx, rx;

static void init(void)
{
	tx.x1 = 123;
	tx.x2 = 11;
	rx.x1 = 1;
	rx.x2 = 1;
}

void EN25Qxxx_selftest(void)
{
	init();
	#if 0
	EN25Qxxx_eraseChip();
	EN25Qxxx_writeData(0x00, &tx, sizeof(tx));
	#else
	EN25Qxxx_readData(0x00, &rx, sizeof(rx));
	DEBUG("rx. = %d, %d\r\n", rx.x1, rx.x2);
	#endif
}
/****************/
