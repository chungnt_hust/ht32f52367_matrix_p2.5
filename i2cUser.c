/*
 * i2cUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "i2cUser.h"
#include "uartUser.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
extern volatile u32 timeDelay;


/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
#define MAX_I2C_BUF_LEN 6

enum
{
	I2C_WRITE = 0,
	I2C_READ,
};

typedef struct
{
	uint8_t index;
	uint8_t len;
	uint8_t dat[MAX_I2C_BUF_LEN];
} i2cData_Typedef;
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static volatile uint16_t bftm_count = 0;
static volatile uint8_t regTemp, rwTemp = I2C_WRITE, txDone = 0, rxDone = 0;
static volatile i2cData_Typedef txTemp, rxTemp;

/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
  * @brief  I2C0 Configuration.
  * @retval None
  ***********************************************************************************************************/
void I2C0_Configuration(void)
{
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
		I2C_InitTypeDef  I2C_InitStructure;
		
    /* Enable I2C0 & AFIO APB clock                                                                     */
    CKCUClock.Bit.I2C0       = 1;
    CKCUClock.Bit.AFIO       = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);

    /* Configure GPIO to I2C0 mode                                                                      */
    AFIO_GPxConfig(I2C0_MASTER_SCK_ID, I2C0_MASTER_SCK_AFIO_PIN, AFIO_FUN_I2C);
		AFIO_GPxConfig(I2C0_MASTER_SDA_ID, I2C0_MASTER_SDA_AFIO_PIN, AFIO_FUN_I2C);
		
    /* I2C0 configuration                                                                                */
    I2C_InitStructure.I2C_GeneralCall = DISABLE;
    I2C_InitStructure.I2C_AddressingMode = I2C_ADDRESSING_7BIT;
    I2C_InitStructure.I2C_Acknowledge = DISABLE;
    I2C_InitStructure.I2C_OwnAddress = SLAVE_ADDRESS;
    I2C_InitStructure.I2C_Speed = I2C0_CLK_SPEED;
    I2C_InitStructure.I2C_SpeedOffset = 0;
    I2C_Init(HT_I2C0, &I2C_InitStructure);

#if(I2C0_USE_INTERRUPT == 1)
		/* Enable I2C Matser interrupts                                                                      */
		I2C_IntConfig(HT_I2C0, I2C_INT_STA | I2C_INT_ADRS | I2C_INT_RXDNE | I2C_INT_TXDE, ENABLE);
		NVIC_EnableIRQ(I2C0_IRQn);
#endif		
    /* Enable I2C0                                                                                       */
    I2C_Cmd(HT_I2C0, ENABLE);
    DEBUG("I2C0 init done\r\n");
}

/*********************************************************************************************************//**
 * @brief   This function handles I2C0 interrupt.
 * @retval  None
 ************************************************************************************************************/
#if(I2C0_USE_INTERRUPT == 1)
void I2C0_IRQHandler(void)
{
  switch(I2C_ReadRegister(HT_I2C0, I2C_REGISTER_SR))
  {
    /*-----------------------------Master Transmitter ------------------------------------------------------*/
    case I2C_MASTER_SEND_START:
//			DEBUG("1");
      break;

    case I2C_MASTER_TRANSMITTER_MODE:
//			DEBUG("2");
      /* Send reg addr 							                                                                        */
      I2C_SendData(HT_I2C0, regTemp);
      break;

    case I2C_MASTER_TX_EMPTY:
      if(rwTemp == I2C_WRITE)
      {
//				DEBUG("3");
				if(txTemp.index < txTemp.len)
					I2C_SendData(HT_I2C0, txTemp.dat[txTemp.index++]);
        else if(txTemp.index == txTemp.len)
				{
					I2C_GenerateSTOP(HT_I2C0);		
					txDone = 1;
				}
      }
      else
      {
//				DEBUG("4");
        /* Send I2C0 re-START & I2C1 slave address for read                                                 */
        I2C_TargetAddressConfig(HT_I2C0, SLAVE_ADDRESS, I2C_MASTER_READ);
      }
      break;

    /*-----------------------------Master Receiver ---------------------------------------------------------*/
    case I2C_MASTER_RECEIVER_MODE:
      /* Enable I2C0 ACK for receiving data                                                                 */
//    DEBUG("5");  
			I2C_AckCmd(HT_I2C0, ENABLE);
		
      break;

    case I2C_MASTER_RX_NOT_EMPTY:
//			DEBUG("6");
      /* Receive data sent from I2C1                                                                        */
      rxTemp.dat[rxTemp.index] = I2C_ReceiveData(HT_I2C0);
			rxTemp.index++;
			if(rxTemp.len == 1)
			{
				I2C_AckCmd(HT_I2C0, DISABLE);
        /* Generate STOP                                                                                    */
				I2C_GenerateSTOP(HT_I2C0);
				rxDone = 1;
			}
			else
			{
				if(rxTemp.index == rxTemp.len - 1)
				{
					/* Disable I2C0 ACK                                                                                 */
					I2C_AckCmd(HT_I2C0, DISABLE);
				}
				else if(rxTemp.index == rxTemp.len)
				{
					/* Generate STOP                                                                                    */
					I2C_GenerateSTOP(HT_I2C0);
					rxDone = 1;
				}
			}
					
      break;

    default:
      break;
  }
}
#endif
/*********************************************************************************************************//**
  * @brief  I2C0 Send one data byte.
  * @retval None
  ***********************************************************************************************************/
void I2C0_Sendbyte(uint8_t Register, uint8_t inData)
{
#if(I2C0_USE_INTERRUPT == 1)	
		/* Send I2C0 START & slave address for write                                                         */
    I2C_TargetAddressConfig(HT_I2C0, SLAVE_ADDRESS, I2C_MASTER_WRITE);
	  regTemp = Register;
		rwTemp = I2C_WRITE;
		txTemp.index = 0;
		txTemp.len = 1;
		txTemp.dat[0] = inData;
	
		timeDelay = 100;
		while(txDone == 0 && timeDelay != 0){};
		if(timeDelay == 0) 
		{
			DEBUG("I2C write time out\r\n");
			return;
		}
		txDone = 0;
#else
		/* Send I2C0 START & slave address for write                                                         */
    I2C_TargetAddressConfig(HT_I2C0, SLAVE_ADDRESS, I2C_MASTER_WRITE);
    /* Check on Master Transmitter STA condition and clear it                                            */
    while (!I2C_CheckStatus(HT_I2C0, I2C_MASTER_SEND_START));
    /* Check on Master Transmitter ADRS condition and clear it                                           */
    while (!I2C_CheckStatus(HT_I2C0, I2C_MASTER_TRANSMITTER_MODE));
		/* Send Register                                                                                     */
    /* Check on Master Transmitter TXDE condition                                                        */
    while (!I2C_CheckStatus(HT_I2C0, I2C_MASTER_TX_EMPTY));
    /* Send I2C0 Register                                                                                */
    I2C_SendData(HT_I2C0, Register);
    
		/* Send data                                                                                         */
    /* Check on Master Transmitter TXDE condition                                                        */
    while (!I2C_CheckStatus(HT_I2C0, I2C_MASTER_TX_EMPTY));
    /* Send I2C0 data                                                                                    */
    I2C_SendData(HT_I2C0, inData);
    
		/* Send I2C0 STOP condition                                                                          */
    I2C_GenerateSTOP(HT_I2C0);
#endif   
//    DEBUG("I2C0 send at add: 0x%02x, data: %d\r\n", Register, inData);
}

/*********************************************************************************************************//**
  * @brief  I2C0 Read one data byte.
  * @retval None
  ***********************************************************************************************************/
uint8_t I2C0_Readbyte(uint8_t Register)
{
    uint8_t outData = 0xFF;

#if(I2C0_USE_INTERRUPT == 1)	 
		/* Send I2C0 START & slave address for write                                                         */
    I2C_TargetAddressConfig(HT_I2C0, SLAVE_ADDRESS, I2C_MASTER_WRITE);
	  regTemp = Register;
		rwTemp = I2C_READ;
		rxTemp.index = 0;
		rxTemp.len = 1;
	
		timeDelay = 100;
		while(rxDone == 0 && timeDelay != 0){};
		if(timeDelay == 0) 
		{
			DEBUG("I2C read time out\r\n");
			return 0xFF;
		}
		rxDone = 0;
		
		DEBUG("I2C0 read at add: 0x%02x, data: 0x%02x\r\n", Register, rxTemp.dat[0]);
		return rxTemp.dat[0];
#else	
		/* Send I2C0 START & slave address for write                                                         */
		I2C_TargetAddressConfig(HT_I2C0, SLAVE_ADDRESS, I2C_MASTER_WRITE);
	/* Check on Master Transmitter STA condition and clear it                                            */
    while (!I2C_CheckStatus(HT_I2C0, I2C_MASTER_SEND_START) || ((HT_I2C0->SR & 0x80) == 0x80));
    /* Check on Master Transmitter ADRS condition and clear it                                           */
    while (!I2C_CheckStatus(HT_I2C0, I2C_MASTER_TRANSMITTER_MODE));
	
    /* Send Register                                                                                     */
    /* Check on Master Transmitter TXDE condition                                                        */
    while (!I2C_CheckStatus(HT_I2C0, I2C_MASTER_TX_EMPTY));
    /* Send I2C0 Register                                                                                */
    I2C_SendData(HT_I2C0, Register);
	
    /* I2C Restart                                                                                       */
    /* Send I2C0 START & slave address for read                                                          */
    I2C_TargetAddressConfig(HT_I2C0, SLAVE_ADDRESS, I2C_MASTER_READ);
    /* Check on Master Transmitter STA condition and clear it                                            */
    while (!I2C_CheckStatus(HT_I2C0, I2C_MASTER_SEND_START));
    /* Check on Master Transmitter ADRS condition and clear it                                           */
    while (!I2C_CheckStatus(HT_I2C0, I2C_MASTER_RECEIVER_MODE));

    /* Read data                                                                                         */
    /* Check on Master Receiver RXDNE condition                                                          */
    while (!I2C_CheckStatus(HT_I2C0, I2C_MASTER_RX_NOT_EMPTY));
    /* Store received data                                                                               */
    outData = I2C_ReceiveData(HT_I2C0);
		
    /* Send I2C0 STOP condition                                                                          */
    I2C_GenerateSTOP(HT_I2C0);
    DEBUG("I2C0 read at add: 0x%02x, data: %d\r\n", Register, outData);
    return outData;
#endif
}

